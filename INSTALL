lacme-accountd depends on the following Perl modules:

  - Config::Tiny
  - Crypt::OpenSSL::RSA (for PEM-encoded key material)
  - Crypt::OpenSSL::Bignum (for PEM-encoded key material)
  - Errno (core module)
  - File::Basename (core module)
  - Getopt::Long (core module)
  - JSON (optionally C/XS-accelerated with JSON::XS)
  - MIME::Base64 (core module)
  - Socket (core module)

On Debian GNU/Linux systems, these dependencies can be installed with
the following command:

    apt-get install libconfig-tiny-perl libcrypt-openssl-rsa-perl libcrypt-openssl-bignum-perl libjson-perl


lacme depends on OpenSSL ≥1.1.0 and the following Perl modules:

  - Config::Tiny
  - Digest::SHA (core module)
  - Date::Parse
  - Errno (core module)
  - Fcntl (core module)
  - File::Basename (core module)
  - File::Temp (core module)
  - Getopt::Long (core module)
  - JSON (optionally C/XS-accelerated with JSON::XS)
  - LWP::UserAgent
  - LWP::Protocol::https (for https:// ACME directory URIs)
  - MIME::Base64 (core module)
  - Net::SSLeay
  - POSIX (core module)
  - Socket (core module)

On Debian GNU/Linux systems, these dependencies can be installed with
the following command:

    apt-get install openssl \
        libconfig-tiny-perl \
        libtimedate-perl \
        libjson-perl \
        libwww-perl \
        liblwp-protocol-https-perl \
        libnet-ssleay-perl \
        libtypes-serialiser-perl

However Debian GNU/Linux users can also use gbp(1) from git-buildpackage
to build their own package:

  $ git checkout debian/latest
  $ AUTO_DEBSIGN=no gbp buildpackage

Alternatively, for the development version:

  $ git checkout debian/latest
  $ git merge master
  $ AUTO_DEBSIGN=no gbp buildpackage --git-force-create --git-upstream-tree=BRANCH
