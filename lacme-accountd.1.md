% lacme-accountd(1)
% [Guilhem Moulin](mailto:guilhem@fripost.org)
% March 2016

Name
====

lacme-accountd - [ACME] client written with process isolation and
minimal privileges in mind (account key manager)

Synopsis
========

`lacme-accountd` [`--config=FILENAME`] [`--privkey=ARG`] [`--socket=PATH`] [`--quiet`]

Description
===========

`lacme-accountd` is the account key manager component of [`lacme`(8)], a
small [ACME] client written with process isolation and minimal
privileges in mind.  No other [`lacme`(8)] component needs access to the
account key; in fact the account key could as well be stored on another
host or a smartcard.

`lacme-accountd` binds to a UNIX-domain socket (specified with
`--socket=`), which [ACME] clients can connect to in order to request
data signatures.
As a consequence, `lacme-accountd` needs to be up and running before
using [`lacme`(8)] to issue [ACME] commands.  Also, the process does not
automatically terminate after the last signature request: instead, one
sends an `INT` or `TERM` [`signal`(7)] to bring the server down.

Furthermore, one can use the UNIX-domain socket forwarding facility of
[OpenSSH] 6.7 and later to run `lacme-accountd` and [`lacme`(8)] on
different hosts.  For instance one could store the account key on a
machine that is not exposed to the internet.  See the
**[examples](#examples)** section below.

Options
=======

`--config=`*filename*

:   Use *filename* as configuration file instead of
    `%E/lacme/lacme-accountd.conf`.  The value is subject to
    [%-specifier expansion](#percent-specifiers).  `lacme-accountd`
    fails when `--config=` is used with a non-existent file, but a
    non-existent default location is treated as if it were an empty
    file.

    See the **[configuration file](#configuration-file)** section below
    for the configuration options.

`--privkey=`*value*

:   Specify the (private) account key to use for signing requests.
    Currently supported *value*s are:

    * `file:`*FILE*, for a private key in PEM format (optionally
      symmetrically encrypted)
    * `gpg:`*FILE*, for a [`gpg`(1)]-encrypted private key

    *FILE* is subject to [%-specifier expansion](#percent-specifiers).

    The [`genpkey`(1ssl)] command can be used to generate a new private
    (account) key:

        $ install -vm0600 /dev/null /path/to/account.key
        $ openssl genpkey -algorithm RSA -out /path/to/account.key

    Currently `lacme-accountd` only supports RSA account keys.

`--socket=`*path*

:   Use *path* as the UNIX-domain socket to bind to for signature
    requests from the [ACME] client.  The value is subject to
    [%-specifier expansion](#percent-specifiers).  `lacme-accountd`
    aborts if *path* exists or if its parent directory is writable by
    other users.
    Default: `%t/S.lacme` (omitting `--socket=` therefore yields an
    error when `lacme-accountd` doesn't run as and the `XDG_RUNTIME_DIR`
    environment variable is unset or empty).

`--stdio`

:   Read signature requests from the standard input and write signatures
    to the standard output, instead of using a UNIX-domain socket for
    communication with the [ACME] client.
    This _internal_ flag should never be used by standalone
    `lacme-accountd` instances, only for those [`lacme`(8)] spawns.

`-h`, `--help`

:   Display a brief help and exit.

`-q`, `--quiet`

:   Be quiet.

`--debug`

:   Turn on debug mode.

Configuration file
==================

When given on the command line, the `--privkey=`, `--socket=` and
`--quiet` options take precedence over their counterpart (without
leading `--`) in the configuration file.  Valid settings are:

*privkey*

:   See `--privkey=`.  This setting is required when `--privkey=` is not
    specified on the command line.

*gpg*

:   For a [`gpg`(1)]-encrypted private account key, specify the binary
    [`gpg`(1)] to use, as well as some default options.
    Default: `gpg --quiet`.

*socket*

:   See `--socket=`.

*logfile*

:   An optional file where to log to.  The value is subject to
    [%-specifier expansion](#percent-specifiers).

*keyid*

:   The "Key ID", as shown by `` `acme account` ``, to give the [ACME]
    client.  With an empty *keyid* (the default) the client forwards the
    JSON Web Key (JWK) to the [ACME] server to retrieve the correct
    value.  A non-empty value therefore saves a round-trip.

    A non-empty value also causes `lacme-accountd` to send an empty JWK,
    thereby revoking all account management access (status change,
    contact address updates etc.) from the client: any `` `acme account` ``
    command (or any command from [`lacme`(8)] before version 0.8.0) is
    bound to be rejected by the [ACME] server.  This provides a
    safeguard against malicious clients.

*quiet*

:   Be quiet. Possible values: `Yes`/`No`.

%-specifiers  {#percent-specifiers}
============

The value the `--config=`, `--privkey=` and `--socket=` CLI options (and
also the *privkey*, *socket* and *logfile* settings from the
configuration file) are subject to %-expansion for the following
specifiers.

----  ------------------------------------------------------------------
`%C`  `@@localstatedir@@/cache` for the root user, and `$XDG_CACHE_HOME`
      for other users (or `$HOME/.cache` if the `XDG_CACHE_HOME`
      environment variable is unset or empty).

`%E`  `@@sysconfdir@@` for the root user, and `$XDG_CONFIG_HOME` for
      other users (or `$HOME/.config` if the `XDG_CONFIG_HOME`
      environment variable is unset or empty).

`%g`  Current group name.

`%G`  Current group ID.

`%h`  Home directory of the current user.

`%t`  `@@runstatedir@@` for the root user, and `$XDG_RUNTIME_DIR` for
      other users.  Non-root users may only use `%t` when the
      `XDG_RUNTIME_DIR` environment variable is set to a non-empty
      value.

`%T`  `$TMPDIR`, or `/tmp` if the `TMPDIR` environment variable is unset
      or empty.

`%u`  Current user name.

`%U`  Current user ID.

`%%`  A literal `%`.
----  ------------------------------------------------------------------

Examples
========

Run `lacme-accountd` in a first terminal:

    $ lacme-accountd --privkey=file:/path/to/account.key --socket=$XDG_RUNTIME_DIR/S.lacme

Then, while `lacme-accountd` is running, execute locally [`lacme`(8)] in
another terminal:

    $ sudo lacme --socket=$XDG_RUNTIME_DIR/S.lacme newOrder

Alternatively, use [OpenSSH] 6.7 or later to forward the socket and
execute [`lacme`(8)] remotely:

    $ ssh -oExitOnForwardFailure=yes -tt -R /path/to/remote.sock:$XDG_RUNTIME_DIR/S.lacme user@example.org \
		sudo lacme --socket=/path/to/remote.sock newOrder

Consult the [`lacme`(8) manual][`lacme`(8)] for a solution involving
connecting to `lacme-accountd` on a dedicated remote host.  Doing so
enables automatic renewal via [`crontab`(5)] or [`systemd.timer`(5)].

See also
========

[`lacme`(8)], [`ssh`(1)]

[ACME]: https://tools.ietf.org/html/rfc8555
[`lacme`(8)]: lacme.8.html
[`signal`(7)]: https://linux.die.net/man/7/signal
[`gpg`(1)]: https://www.gnupg.org/documentation/manpage.en.html
[OpenSSH]: https://www.openssh.com/
[`ssh`(1)]: https://man.openbsd.org/ssh
[`genpkey`(1ssl)]: https://www.openssl.org/docs/manmaster/man1/openssl-genpkey.html
[`crontab`(5)]: https://linux.die.net/man/5/crontab
[`systemd.timer`(5)]: https://www.freedesktop.org/software/systemd/man/systemd.timer.html
