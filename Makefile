DESTDIR ?= /usr/local
BUILDDIR ?= ./build
MANUAL_FILES = $(addprefix $(BUILDDIR)/,$(patsubst ./%.md,%,$(wildcard ./*.[1-9].md)))

all: manual $(addprefix $(BUILDDIR)/,lacme lacme-accountd client webserver \
	$(wildcard certs/* config/* snippets/*) \
	certs/ca-certificates.crt certs-staging/ca-certificates.crt)
doc: manual

manual: $(MANUAL_FILES)

# upper case the headers and remove the links
$(MANUAL_FILES): $(BUILDDIR)/%: $(BUILDDIR)/%.md
	pandoc -f markdown -t json -- "$<" | ./pandoc2man.jq | pandoc -s -f json -t man -o "$@"

# used for validation, see https://letsencrypt.org/certificates/
$(BUILDDIR)/certs/ca-certificates.crt: \
        certs/isrgrootx1.pem \
        certs/isrg-root-x2.pem
	mkdir -pv -- $(@D)
	cat -- $^ >$@

# Staging Environment for tests, see https://letsencrypt.org/docs/staging-environment/
$(BUILDDIR)/certs-staging/ca-certificates.crt: \
        certs-staging/letsencrypt-stg-root-x[12].pem
	mkdir -pv -- $(@D)
	cat -- $^ >$@

prefix ?= $(DESTDIR)
exec_prefix ?= $(prefix)
bindir ?= $(exec_prefix)/bin
sbindir ?= $(exec_prefix)/sbin
libexecdir ?= $(exec_prefix)/libexec
datarootdir ?= $(prefix)/share
datadir ?= $(datarootdir)
sysconfdir ?= $(prefix)/etc
localstatedir ?= $(prefix)/var
runstatedir ?= $(localstatedir)/run
mandir ?= $(datarootdir)/man
man1dir ?= $(mandir)/man1
man8dir ?= $(mandir)/man8

lacme_www_user ?= www-data
lacme_www_group ?= www-data
lacme_client_user ?= nobody
lacme_client_group ?= nogroup

acmeapi_server ?= https://acme-v02.api.letsencrypt.org/directory

$(BUILDDIR)/%: %
	mkdir -pv -- $(@D)
	cp --no-dereference --preserve=mode,links,xattr -vfT -- "$<" "$@"
	sed -i "s#@@bindir@@#$(bindir)#g; \
	        s#@@sbindir@@#$(sbindir)#g; \
	        s#@@libexecdir@@#$(libexecdir)#g; \
	        s#@@datadir@@#$(datadir)#g; \
	        s#@@localstatedir@@#$(localstatedir)#g; \
	        s#@@runstatedir@@#$(runstatedir)#g; \
	        s#@@sysconfdir@@#$(sysconfdir)#g; \
	        s#@@lacme_www_user@@#$(lacme_www_user)#g; \
	        s#@@lacme_www_group@@#$(lacme_www_group)#g; \
	        s#@@lacme_client_user@@#$(lacme_client_user)#g; \
	        s#@@lacme_client_group@@#$(lacme_client_group)#g; \
	        s#@@acmeapi_server@@#$(acmeapi_server)#g; \
	        " -- "$@"

release:
	@if ! git diff HEAD --quiet -- ./Changelog ./lacme ./lacme-accountd ./client; then \
		echo "Dirty state, refusing to release!" >&2; \
		exit 1; \
	fi
	VERS=$$(dpkg-parsechangelog -l Changelog -SVersion 2>/dev/null) && \
		if git rev-parse -q --verify "refs/tags/v$$VERS" >/dev/null; then echo "tag exists" 2>/dev/null; exit 1; fi && \
		sed -ri "0,/^( -- .*)  .*/ s//\\1  $(shell date -R)/" ./Changelog && \
		sed -ri "0,/^(our\\s+\\\$$VERSION\\s*=\\s*)'[0-9.]+'\\s*;/ s//\\1'$$VERS';/" \
			-- ./lacme ./lacme-accountd ./client && \
		git commit -m "Prepare new release v$$VERS." \
			-- ./Changelog ./lacme ./lacme-accountd ./client && \
		git tag -sm "Release version $$VERS" "v$$VERS"

install: all
	install -m0644 -vDt $(sysconfdir)/lacme -- $(BUILDDIR)/config/*.conf $(BUILDDIR)/snippets/*.conf
	install -m0755 -vd -- $(sysconfdir)/lacme/lacme-certs.conf.d
	install -m0644 -vDt $(datadir)/lacme -- $(BUILDDIR)/certs/*
	install -m0755 -vDt $(libexecdir)/lacme -- $(BUILDDIR)/client $(BUILDDIR)/webserver
	install -m0644 -vDt $(man1dir) -- $(BUILDDIR)/lacme-accountd.1
	install -m0644 -vDt $(man8dir) -- $(BUILDDIR)/lacme.8
	install -m0755 -vDt $(sbindir) -- $(BUILDDIR)/lacme
	install -m0755 -vDt $(bindir) -- $(BUILDDIR)/lacme-accountd
	install -m0755 -vdD -- $(sysconfdir)/apache2/conf-available
	ln -sv -- ../../lacme/apache2.conf $(sysconfdir)/apache2/conf-available/lacme.conf

uninstall:
	rm -vf -- $(bindir)/lacme-accountd $(sbindir)/lacme
	rm -vf -- $(man1dir)/lacme-accountd.1 $(man8dir)/lacme.8
	rm -rvf -- $(sysconfdir)/lacme $(datadir)/lacme $(libexecdir)/lacme
	rm -vf -- $(sysconfdir)/apache2/conf-available/lacme.conf

clean:
	rm -rvf -- $(BUILDDIR)

.PHONY: all doc manual release install uninstall clean
