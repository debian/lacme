lacme (0.8.3-1) unstable; urgency=high

  * New upstream bugfix release.
    + Fix post-issuance validation logic.  We avoid pinning the intermediate
      certificates in the bundle and instead validate the leaf certificate
      with intermediates supplied during issuance as untrusted (used for chain
      building only).  Only the root certificates are used as trust anchor.
      Not pinning intermediate certificates is in line with Let's Encrypt's
      latest recommendations.
      Closes: #1072847
    + Pass `-in /dev/stdin` option to openssl(1) to avoid warning with OpenSSL
      3.2 or later.
    + Fix test suite to account for Let's Encrypt's (staging) ACME server
      changes.
  * d/control: Update Standards-Version to 4.7.0 (no changes necessary).

 -- Guilhem Moulin <guilhem@debian.org>  Thu, 13 Jun 2024 17:56:33 +0200

lacme (0.8.2-1) unstable; urgency=medium

  * New upstream bugfix release.
    + client: Handle "ready" → "processing" → "valid" status change during
      newOrder, instead of just "ready" → "valid".  The latter may be what we
      observe when the server is fast enough, but according to RFC 8555 sec.
      7.1.6 the state actually transitions via "processing" state and we need
      to account for that.  Closes: #1034834.
    + Test suite: Point stretch's archive URL to archive.d.o.

 -- Guilhem Moulin <guilhem@debian.org>  Tue, 25 Apr 2023 20:08:21 +0200

lacme (0.8.1-1) unstable; urgency=medium

  [ Guilhem Moulin ]
  * New upstream bugfix release.
  * Salsa CI: Remove default configuration file.
  * d/control: Improve long package descriptions.
  * Set field Upstream-Name in debian/copyright.
  * Update standards version to 4.6.2, no changes needed.

  [ Debian Janitor ]
  * d/control: Add 'Multi-Arch: foreign' mark.

 -- Guilhem Moulin <guilhem@debian.org>  Wed, 25 Jan 2023 03:33:11 +0100

lacme (0.8.0-2) unstable; urgency=medium

  * d/lacme.postrm: Don't delete system users on purge.  There might be files
    on disk owned by _lacme-client when 'challenge-directory' is set in the
    configuration (closes: #988032).

 -- Guilhem Moulin <guilhem@debian.org>  Tue, 04 May 2021 01:37:13 +0200

lacme (0.8.0-1) unstable; urgency=low

  * New upstream release (closes: #970458, #970800, #972456).
  * The internal webserver now runs as a dedicated system user _lacme-www
    (and group nogroup) instead of www-data:www-data.  This is configurable
    in the [webserver] section of the lacme(8) configuration file.
  * The internal ACME client now runs as a dedicated system user _lacme-client
    (and group nogroup) instead of nobody:nogroup.  This is configurable in
    the [client] section of the lacme(8) configuration file.
  * The _lacme-www and _lacme-client system users are created automatically by
    lacme.postinst (hence a new Depends: adduser), and deleted on purge.  (So
    make sure not to chown any file to these internal users.)
  * d/control: New lacme-accountd Suggests: openssl, gpg (for account key
    generation and decryption).
  * Add d/upstream/signing-key.asc, the OpenPGP used to signed upstream tags.
  * d/control: Bump Standards-Version to 4.5.1 (no changes necessary).
  * Add d/watch pointing to the upstream repository.
  * d/gbp.conf: Update upstream tag template.
  * d/gbp.conf: Update debian and upstream branches in compliance with DEP-14.
  * d/control: Point Vcs-* to salsa.
  * Add debian/salsa-ci.yml file.
  * d/.gitattributes: New file to merge d/changelog with dpkg-mergechangelogs.
  * Add d/upstream/metadata with Repository and Repository-Browse.
  * d/control: Remove libtypes-serialiser-perl from lacme's Depends.
  * d/control: lacme now require openssl 1.1.0 or later.
  * d/copyright: Bump copyright years.
  * d/copyright: Point Source: to the upstream repository.
  * d/control: lacme recommends lacme-accountd 0.8.0-1 or later.
  * d/lacme.links: Remove /etc/apache2/conf-available/lacme.conf, now part of
    the upstream build system.
  * d/lacme.install: include new configuration files and snippets.

 -- Guilhem Moulin <guilhem@debian.org>  Mon, 22 Feb 2021 03:31:23 +0100

lacme (0.7-1) unstable; urgency=high

  * New upstream release.  Closes: #975862.

 -- Guilhem Moulin <guilhem@debian.org>  Thu, 26 Nov 2020 00:05:55 +0100

lacme (0.6.1-1) unstable; urgency=medium

  * New upstream release.  Closes: #955767, #966958.
    + Default listening socket for the webserver component is now
      /run/lacme-www.socket.  (It was previously under the legacy directory
      /var/run.)
  * debian/*: Adapt to new build system.
  * debian/control: Bump debhelper compatibility level to 13.

 -- Guilhem Moulin <guilhem@debian.org>  Tue, 04 Aug 2020 01:43:05 +0200

lacme (0.6-3) unstable; urgency=medium

  * New symlink /etc/apache2/conf-available/lacme.conf pointing to
    /etc/lacme/apache2.conf for use with the a2enconf/a2disconf interface.
    (Closes: #955859.)
  * debian/*.{install,manpages}: Copy files from $DESTDIR (debian/tmp) not
    from the source tree.
  * debian/control:
    + Add "Rules-Requires-Root: no".
    + Add "debhelper-compat (= 12)" to Build-Depends.
    + Bump Standards-Version to 4.5.0 (no changes necessary).
  * Rename debian/source.lintian-overrides to debian/source/lintian-overrides.

 -- Guilhem Moulin <guilhem@debian.org>  Sun, 05 Apr 2020 18:26:36 +0200

lacme (0.6-2) unstable; urgency=medium

  * d/control: new dependency for lacme: libtimedate-perl.  (It's currently a
    reverse dependency of LWP, but we use it explicitly.)

 -- Guilhem Moulin <guilhem@debian.org>  Wed, 18 Sep 2019 15:41:03 +0200

lacme (0.6-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Bump Standards-Version to 4.4.0 (no changes necessary).
  * d/compat, d/control: Bump debhelper compatibility level to 12.

 -- Guilhem Moulin <guilhem@debian.org>  Wed, 21 Aug 2019 23:50:15 +0200

lacme (0.5-1) unstable; urgency=medium

  * New upstream release, adding support for v2 ACME endpoints.
  * Fix manpage generation with pandoc >=2.1.  (Closes: #896982.)
  * debian/control:
    + Bump Standards-Version to 4.1.4.  No changes.
    + Build-depends: bump minimum pandoc version to 2.1.
    + Depends (lacme): add libtypes-serialiser-perl

 -- Guilhem Moulin <guilhem@debian.org>  Wed, 09 May 2018 14:17:19 +0200

lacme (0.4-1) unstable; urgency=medium

  * Fix manpage generation with pandoc >=1.18.  (Closes: #869885.)

 -- Guilhem Moulin <guilhem@debian.org>  Fri, 28 Jul 2017 00:24:06 +0200

lacme (0.3-1) unstable; urgency=low

  * New upstream release.
  * Provide apache2 and nginx configuration snippet in /etc/lacme.
  * debian/control: Bump Standards-Version to 4.0.0.  No changes.

 -- Guilhem Moulin <guilhem@debian.org>  Sun, 09 Jul 2017 00:41:23 +0200

lacme (0.2-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Promote lacme-accountd from lacme's Suggests to Recommends.
    + Bump Standards-Version to 3.9.8.  No changes.

 -- Guilhem Moulin <guilhem@guilhem.org>  Mon, 05 Dec 2016 16:35:59 +0100

lacme (0.1-1) unstable; urgency=low

  * Initial release.  (Closes: #827357, #827358.)

 -- Guilhem Moulin <guilhem@guilhem.org>  Tue, 08 Dec 2015 18:58:20 +0100
